﻿Shader "Custom/Laser"
{
    Properties
    {
        _MainTex ("Laser texture", 2D) = "white" {}
        _DisText ("Distorsion texture", 2D) = "white" {}
        [HDR]_Color ("Laser color", Color) = (1, 1, 1, 1)
        _Speed("Laser speed", Range(1, 5)) = 1
        _DisValue("Distorsion value", Range(1, 50)) = 1
    }
    SubShader
    {
        Tags { "RenderType"="Transparent" "Queue" = "Transparent"}
        LOD 100

        Blend SrcAlpha OneMinusSrcAlpha

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                float4 vertex : SV_POSITION;
            };

            sampler2D _MainTex;
            sampler2D _DisTex;
            float4 _MainTex_ST;
            float _Speed;
            float4 _Color;
            float _DisValue;

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = TRANSFORM_TEX(v.uv, _MainTex);
                
                return o;
            }

            fixed4 frag (v2f i) : SV_Target
            {
                
                half distorsion = tex2D(_DisTex, i.uv + (_Time * _Speed)).r;
                i.uv.x += distorsion / _DisValue;
                i.uv.y += distorsion / _DisValue;
                // sample the texture
                fixed4 col = tex2D(_MainTex, i.uv += _Time.y * _Speed);

                return col * _Color;
            }
            ENDCG
        }
    }
}
