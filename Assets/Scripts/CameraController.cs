﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

public class CameraController : MonoBehaviour
{

    private CinemachineVirtualCamera vCam;
    private CinemachineOrbitalTransposer orbital;

    public GameObject target;
    public float camSpeed;

    // Start is called before the first frame update
    void Start()
    {
        vCam = GetComponent<CinemachineVirtualCamera>();
        orbital = vCam.GetCinemachineComponent<CinemachineOrbitalTransposer>();
    }

    // Update is called once per frame
    void Update()
    {
        if(target != null)
        {
            if (target.active)
            {
                if (Input.GetKey(KeyCode.E))
                {
                    orbital.m_Heading.m_Bias += Time.deltaTime * camSpeed;
                }

                if (Input.GetKey(KeyCode.Q))
                {
                    orbital.m_Heading.m_Bias -= Time.deltaTime * camSpeed;
                }
            }
        }
    }
}
