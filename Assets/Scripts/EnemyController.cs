﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyController : MonoBehaviour
{
    public GameObject powerUp, pointToGo, shootPosition, bulletPrefab, projectilePrefab, explosionPrefab, soundPUPrefab, soundExplosionPrefab, soundShootPrefab;
    public Weapons enemyWeapon;
    public LayerMask layerMask, obsLayer;
    public Health healthController;
    public EnemyHealthBar hbar;

    public float speed;

    private Rigidbody rigi;
    private Animation anim;
    private PlayerController pc;
    private GameObject target, player;
    private Vector3 posToGo;

    private enum States { shoot, walk, dead }
    private States state;

    private bool canShoot;
    private float contador, contadorMax, cadenceTimer;

    // Start is called before the first frame update
    void Start()
    {
        enemyWeapon = new Weapons();
        enemyWeapon.weapon = Weapons.WeaponType.pistol;

        rigi = GetComponent<Rigidbody>();
        anim = GetComponent<Animation>();

        state = States.shoot;
        canShoot = true;

        cadenceTimer = 10f;
        contador = 0;
        contador = Random.Range(1f, 4f);

        player = GameObject.FindGameObjectWithTag("Player");

        if (player != null)
        {
            pc = player.GetComponent<PlayerController>();
            target = player;
        }

        healthController = new Health();
        healthController.hBar = hbar;

        if (gameObject.tag.Equals("DistanceEnemy"))
        {
            anim.Play("shoot");
            healthController.health = 80;
        }
        else
        {
            healthController.health = 40;
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (!healthController.dead && target != null)
        {
            transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(new Vector3(target.transform.position.x, transform.position.y, target.transform.position.z) - transform.position), Time.deltaTime * 9f);
        }

        if (gameObject.tag.Equals("DistanceEnemy"))
        {
            if (cadenceTimer <= enemyWeapon.GetCadency())
            {
                cadenceTimer += Time.deltaTime;
                canShoot = false;
            }
            else
            {
                canShoot = true;
            }

            pointToGo.transform.position = posToGo;
        }
        else
        {
            if(state != States.dead)
            {
                state = States.walk;
            }
        }

        if (healthController.health <= 0)
        {
            if (gameObject.tag.Equals("BombEnemy") && target != null)
            {
                pc.AddScore(25f);
            }

            state = States.dead;
        }

        switch (state)
        {
            case States.shoot:
                Shoot();
                break;
            case States.walk:
                Walk();
                break;
            case States.dead:
                Dead();
                break;
        }

        if(player == null)
        {
            Destroy(gameObject);
        }
    }

    private void Shoot()
    {
        //FreezePosition();
        //anim.Play("Shoot");
        target = player;

        rigi.velocity = new Vector3(0, 0, 0);

        if (target != null)
        {
            if (Physics.Linecast(transform.position, target.transform.position, obsLayer))
            {
                contador = 0;
                contadorMax = Random.Range(1f, 4f);

                CalculateNewPosition();
            }
            else
            {
                if (powerUp != null)
                {
                    state = States.walk;
                    posToGo = powerUp.transform.position;
                    target = powerUp;

                    contador = 0;
                    contadorMax = Random.Range(1f, 4f);
                }
                else
                {
                    if (contador <= contadorMax)
                    {
                        contador += Time.deltaTime;
                        rigi.velocity = new Vector3(0, 0, 0);

                        if (canShoot)
                        {
                            GameObject soundShoot = Instantiate(soundShootPrefab, transform.position, transform.rotation);
                            Destroy(soundShoot, 1f);

                            GameObject projectile = Instantiate(projectilePrefab, shootPosition.transform.position, shootPosition.transform.rotation);
                            Destroy(projectile, 1);

                            if (enemyWeapon.weapon == Weapons.WeaponType.shotgun)
                            {
                                GameObject bullet01 = Instantiate(bulletPrefab, shootPosition.transform.position, shootPosition.transform.rotation * Quaternion.Euler(0, Random.Range(-4f, -9), 0));
                                bullet01.GetComponent<Bullet>().damageValue(enemyWeapon.GetDamage());
                                bullet01.tag = "enemyBullet";

                                GameObject bullet02 = Instantiate(bulletPrefab, shootPosition.transform.position, shootPosition.transform.rotation * Quaternion.Euler(0, Random.Range(-4f, -9), 0));
                                bullet02.GetComponent<Bullet>().damageValue(enemyWeapon.GetDamage());
                                bullet02.tag = "enemyBullet";

                                GameObject bullet03 = Instantiate(bulletPrefab, shootPosition.transform.position, shootPosition.transform.rotation);
                                bullet03.GetComponent<Bullet>().damageValue(enemyWeapon.GetDamage());
                                bullet03.tag = "enemyBullet";

                                GameObject bullet04 = Instantiate(bulletPrefab, shootPosition.transform.position, shootPosition.transform.rotation * Quaternion.Euler(0, Random.Range(4f, 9), 0));
                                bullet04.GetComponent<Bullet>().damageValue(enemyWeapon.GetDamage());
                                bullet04.tag = "enemyBullet";

                                GameObject bullet05 = Instantiate(bulletPrefab, shootPosition.transform.position, shootPosition.transform.rotation * Quaternion.Euler(0, Random.Range(4f, 9), 0));
                                bullet05.GetComponent<Bullet>().damageValue(enemyWeapon.GetDamage());
                                bullet05.tag = "enemyBullet";

                                anim.Play("shoot");
                            }
                            else
                            {
                                GameObject bullet = Instantiate(bulletPrefab, shootPosition.transform.position, shootPosition.transform.rotation * Quaternion.Euler(0, Random.Range(-3f, 4f), 0));
                                bullet.GetComponent<Bullet>().damageValue(enemyWeapon.GetDamage());
                                bullet.tag = "enemyBullet";

                                anim.Stop();
                                anim.Play("shoot");
                            }

                            cadenceTimer = 0;
                        }
                    }
                    else
                    {
                        contador = 0;
                        contadorMax = Random.Range(1f, 4f);

                        CalculateNewPosition();
                    }
                }
            }        
        }
    }

    private void Walk()
    {
        if (gameObject.tag.Equals("DistanceEnemy"))
        {
            anim.Play("walk");
        }

        rigi.velocity = transform.TransformDirection(Vector3.forward);
        rigi.velocity = Vector3.Normalize(rigi.velocity) * speed;

        if (!gameObject.tag.Equals("BombEnemy"))
        {
            if (Vector3.Distance(transform.position, pointToGo.transform.position) <= 4f && gameObject.tag.Equals("DistanceEnemy") || target == null && gameObject.tag.Equals("DistanceEnemy"))
            {
                state = States.shoot;
            }
        }
        else
        {            
            if(target != null)
            {
                if (Vector3.Distance(transform.position, target.transform.position) <= 3f)
                {
                    PlayerController pc = target.GetComponent<PlayerController>();
                    pc.rigi.AddExplosionForce(450f, transform.position, 5f, 5f, ForceMode.Impulse);
                    pc.healthController.TakeDamage(35f);
                    state = States.dead;
                }
            }
        }
    }

    private void Dead()
    {
        //FreezePosition();
        rigi.velocity = new Vector3(0, 0, 0);

        if (gameObject.tag.Equals("BombEnemy"))
        {
            LSDead();
        }
        else
        {
            Collider eColi = GetComponent<Collider>();
            eColi.enabled = false;

            target = null;
            anim.Play("death");
        }
    }

    void LSDead()
    {
        GameObject soundExplosion = Instantiate(soundExplosionPrefab, transform.position, transform.rotation);
        Destroy(soundExplosion, 1f);

        GameObject explosion = Instantiate(explosionPrefab, transform.position, transform.rotation);

        if (gameObject.tag.Equals("DistanceEnemy"))
        {
            pc.AddScore(50f);
        }

        Destroy(explosion, 2.1f);
        Destroy(gameObject);
    }

    public void SetPowerUp(GameObject obj)
    {
        powerUp = obj;
    }

    void CalculateNewPosition()
    {
        posToGo = new Vector3(transform.position.x + Random.Range(-12, 13), transform.position.y, transform.position.z + Random.Range(-12, 13));
        pointToGo.transform.position = posToGo;

        RaycastHit hit;

        if (Physics.Raycast(posToGo, pointToGo.transform.TransformDirection(Vector3.down), out hit, Mathf.Infinity, layerMask) && !Physics.Linecast(transform.position, posToGo, obsLayer))
        {
            target = pointToGo;
            state = States.walk;            
        }
        else
        {
            pointToGo.transform.position = transform.position;
            state = States.shoot;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (gameObject.tag.Equals("DistanceEnemy") && other.gameObject.tag.Equals("PowerUp"))
        {            
            GameObject soundPU = Instantiate(soundPUPrefab, transform.position, transform.rotation);
            Destroy(soundPU, 1f);

            PowerUpController puc = other.gameObject.GetComponent<PowerUpController>();
            enemyWeapon.weapon = puc.GetWeapon();
            Destroy(other.gameObject);            
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (gameObject.tag.Equals("BombEnemy"))
        {
            if (collision.gameObject.tag.Equals("Shield"))
            {
                GameObject soundExplosion = Instantiate(soundExplosionPrefab, transform.position, transform.rotation);
                Destroy(soundExplosion, 1f);

                GameObject explosion = Instantiate(explosionPrefab, transform.position, transform.rotation);

                Destroy(explosion, 2.1f);
                Destroy(gameObject);
            }
        }
    }
}