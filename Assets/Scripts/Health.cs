﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Health : MonoBehaviour
{
    public float health;
    public bool dead, invencible;

    public EnemyHealthBar hBar;
    public PlayerHealthBar pHBar;

    public float GetHealth()
    {
        return health;
    }

    public void TakeDamage(float damage)
    {
        if (!invencible)
        {
            health -= damage;

            if(hBar != null)
            {
                hBar.SetHealth(health);
            }

            if(pHBar != null)
            {
                pHBar.SetHealth(health);
            }
        }
    }

    public void Heal(float heal)
    {
        health += heal;

        if(health >= 200)
        {
            health = 200;
        }

        pHBar.SetHealth(health);
    }
}