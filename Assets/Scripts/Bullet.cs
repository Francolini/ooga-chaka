﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    private Rigidbody rigi;

    private float bulletDamage;

    public float speed;

    public GameObject impactPrefab, soundPlayerHurtPrefab;
    public GameObject soundEnemyHurtPrefab;

    // Start is called before the first frame update
    void Awake()
    {
        rigi = GetComponent<Rigidbody>();
        Destroy(gameObject, 2.5f);

        bulletDamage = 0;
    }

    // Update is called once per frame
    void Update()
    {
        transform.Translate(Vector3.forward * Time.deltaTime * speed);
    }

    private void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.tag.Equals("Player") && gameObject.tag.Equals("enemyBullet"))
        {
            GameObject soundPlayerHurt = Instantiate(soundPlayerHurtPrefab, transform.position, transform.rotation);
            Destroy(soundPlayerHurt, 1f);

            other.gameObject.GetComponent<PlayerController>().healthController.TakeDamage(bulletDamage);
            DestroyBullet();
        }

        if (other.gameObject.tag.Equals("DistanceEnemy") && gameObject.tag.Equals("playerBullet") || other.gameObject.tag.Equals("BombEnemy") && gameObject.tag.Equals("playerBullet"))
        {
            GameObject soundEnemyHurt = Instantiate(soundEnemyHurtPrefab, transform.position, transform.rotation);
            Destroy(soundEnemyHurt, 1f);

            other.gameObject.GetComponent<EnemyController>().healthController.TakeDamage(bulletDamage);
            DestroyBullet();
        }

        if (other.gameObject.tag.Equals("Obstacle"))
        {
            DestroyBullet();
        }

        if (other.gameObject.tag.Equals("Shield"))
        {
            DestroyBullet();
        }
    }

    public void damageValue(float dmg)
    {
        bulletDamage = dmg;
    }

    private void DestroyBullet()
    {
        GameObject impact = Instantiate(impactPrefab, transform.position, transform.rotation);
        Destroy(impact, 1);
        Destroy(gameObject);
    }
}