﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShieldBar : MonoBehaviour
{
    public Slider slider;

    private void LateUpdate()
    {
        transform.LookAt(transform.position + Camera.main.transform.forward);
        slider.value -= Time.deltaTime;
    }
}
