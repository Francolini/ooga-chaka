﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerUpController : MonoBehaviour
{
    public Material shoot, machine, life, shield;

    private Weapons.WeaponType weapon;
    private MeshRenderer matRenderer;

    private int randomWeapon;

    // Start is called before the first frame update
    void Start()
    {
        matRenderer = GetComponent<MeshRenderer>();

        //Destroy(gameObject, 10f);
        randomWeapon = Random.Range(1, 5);

        switch (randomWeapon)
        {
            case 1:
                matRenderer.material = machine;
                weapon = Weapons.WeaponType.machineGun;
                break;

            case 2:
                matRenderer.material = shoot;
                weapon = Weapons.WeaponType.shotgun;
                break;

            case 3:
                matRenderer.material = life;
                gameObject.tag = "PULife";
                break;
            case 4:
                matRenderer.material = shield;
                gameObject.tag = "PUShield";
                break;
        }
    }

    public Weapons.WeaponType GetWeapon()
    {
        return weapon;
    }
}