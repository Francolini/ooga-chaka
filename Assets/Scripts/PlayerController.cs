﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour
{
    private Vector3 forward, right, rightMove, forwardMove, moveDirection;
    private Weapons playerWeapon;
    private GameObject temporalShield;

    private enum MotorStates { idle, move, dead }
    private MotorStates motorStates;

    private enum RobotStates { idle, shoot, dead }
    private RobotStates robotStates;

    private bool canShoot;  
    private float horizontal, vertical, xRotation, yRotation, cadenceTimer, recail;

    public Rigidbody rigi;
    public GameObject robotBody, motorBody, bulletPrefab, projectilePrefab, shootPosition, explosionPrefab, shieldPrefab, soundShootPrefab, soundDeadPrefab, soundPUPrefab, soundExplosionPrefab;
    public GameObject[] shotGunBullets;
    public Health healthController;
    public GUIController gui;
    public Text scoreTxt, healthTxt;
    public Animator anim;
    public LayerMask enemyLayer;
    public PlayerHealthBar pHB;
    public MusicController mc;

    public float speed, score;

    // Start is called before the first frame update
    void Start()
    {
        rigi = GetComponent<Rigidbody>();
        // motorAnim = motorBody.GetComponent<Animator>(); 
        anim.Play("Idle");

        motorStates = MotorStates.idle;
        robotStates = RobotStates.idle;

        xRotation = 0f;
        yRotation = 0f;

        cadenceTimer = 10f;
        recail = 0;
        canShoot = true;

        playerWeapon = new Weapons();
        playerWeapon.weapon = Weapons.WeaponType.pistol;

        healthController = new Health();
        healthController.health = 200;
        healthController.pHBar = pHB;

        scoreTxt.text = score.ToString();
        //healthTxt.text = healthController.health.ToString();
    }

    // Update is called once per frame
    void Update()
    {
        horizontal = Input.GetAxisRaw("Horizontal");
        vertical = Input.GetAxisRaw("Vertical");

        switch (motorStates)
        {
            case MotorStates.idle:
                MotorIdle();
                break;
            case MotorStates.move:
                Run();
                break;
            case MotorStates.dead:
                Dead();
                break;
        }

        switch (robotStates)
        {
            case RobotStates.idle:
                RobotIdle();
                break;
            case RobotStates.shoot:
                Shoot();
                break;
            case RobotStates.dead:
                Dead();
                break;
        }

        if (cadenceTimer <= playerWeapon.GetCadency())
        {
            cadenceTimer += Time.deltaTime;
            canShoot = false;
        }
        else
        {
            canShoot = true;
        }

        if(healthController.health <= 0)
        {
            robotStates = RobotStates.dead;
            motorStates = MotorStates.dead;
        }

        forward = Camera.main.transform.forward;
        forward.y = 0;
        forward = Vector3.Normalize(forward);

        right = Quaternion.Euler(new Vector3(0, 90, 0)) * forward;

        Ray cameraRay = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;

        if (Physics.Raycast(Camera.main.transform.position, cameraRay.direction, out hit, Mathf.Infinity, enemyLayer))
        {
            Vector3 pointToLook = hit.point;
            pointToLook = new Vector3(pointToLook.x, transform.position.y, pointToLook.z);

            Quaternion lookAt = Quaternion.LookRotation(new Vector3(pointToLook.x, robotBody.transform.position.y, pointToLook.z) - robotBody.transform.position);
            Quaternion.Normalize(lookAt);

            robotBody.transform.rotation = Quaternion.Slerp(robotBody.transform.rotation, lookAt, Time.deltaTime * 25f);
        }
        else
        {
            Plane groundPlane = new Plane(Vector3.up, Vector3.zero);
            float rayLength;

            if (groundPlane.Raycast(cameraRay, out rayLength))
            {
                Vector3 pointToLook = cameraRay.GetPoint(rayLength);
                Debug.DrawLine(cameraRay.origin, pointToLook, Color.cyan);

                Quaternion lookAt = Quaternion.LookRotation(new Vector3(pointToLook.x, robotBody.transform.position.y, pointToLook.z) - robotBody.transform.position);

                robotBody.transform.rotation = Quaternion.Slerp(robotBody.transform.rotation, lookAt, Time.deltaTime * 25f);
            }
        }
    }

    void MotorIdle()
    {
        rigi.velocity = new Vector3(0, rigi.velocity.y, 0);
        motorBody.transform.rotation = Quaternion.Slerp(motorBody.transform.rotation, Quaternion.Euler(new Vector3(xRotation, yRotation, 0)), Time.deltaTime * 15f);

        if (horizontal != 0 || vertical != 0)
        {
            motorStates = MotorStates.move;
            xRotation = 60f;
        }
    }

    void Run()
    {
        CheckDirection();
        motorBody.transform.rotation = Quaternion.Slerp(motorBody.transform.rotation, Quaternion.Euler(new Vector3(xRotation, yRotation, 0)), Time.deltaTime * 15f);

        Move();

        if (horizontal == 0 && vertical == 0)
        {
            motorStates = MotorStates.idle;
            xRotation = 0f;
        }
    }

    void RobotIdle()
    {
        if (Input.GetButton("Fire1")) robotStates = RobotStates.shoot;
    }

    void Shoot()
    {
        if (canShoot)
        {
            GameObject projectile = Instantiate(projectilePrefab, shootPosition.transform.position, shootPosition.transform.rotation);
            Destroy(projectile, 1);

            GameObject soundShoot = Instantiate(soundShootPrefab, transform.position, transform.rotation);
            Destroy(soundShoot, 1f);

            if (playerWeapon.weapon == Weapons.WeaponType.shotgun)
            {
                GameObject bullet01 = Instantiate(bulletPrefab, shootPosition.transform.position, shootPosition.transform.rotation * Quaternion.Euler(0, Random.Range(-2.5f, -7), 0));
                bullet01.GetComponent<Bullet>().damageValue(playerWeapon.GetDamage());
                bullet01.tag = "playerBullet";
                bullet01.layer = 16;

                GameObject bullet02 = Instantiate(bulletPrefab, shootPosition.transform.position, shootPosition.transform.rotation * Quaternion.Euler(0, Random.Range(-2.5f, -7), 0));
                bullet02.GetComponent<Bullet>().damageValue(playerWeapon.GetDamage());
                bullet02.tag = "playerBullet";
                bullet02.layer = 16;

                GameObject bullet03 = Instantiate(bulletPrefab, shootPosition.transform.position, shootPosition.transform.rotation);
                bullet03.GetComponent<Bullet>().damageValue(playerWeapon.GetDamage());
                bullet03.tag = "playerBullet";
                bullet03.layer = 16;

                GameObject bullet04 = Instantiate(bulletPrefab, shootPosition.transform.position, shootPosition.transform.rotation * Quaternion.Euler(0, Random.Range(2.5f, 7), 0));
                bullet04.GetComponent<Bullet>().damageValue(playerWeapon.GetDamage());
                bullet04.tag = "playerBullet";
                bullet04.layer = 16;

                GameObject bullet05 = Instantiate(bulletPrefab, shootPosition.transform.position, shootPosition.transform.rotation * Quaternion.Euler(0, Random.Range(2.5f, 7), 0));
                bullet05.GetComponent<Bullet>().damageValue(playerWeapon.GetDamage());
                bullet05.tag = "playerBullet";
                bullet05.layer = 16;

                anim.Play("Attack");
            }
            else
            {
                if(playerWeapon.weapon == Weapons.WeaponType.machineGun)
                {
                    recail = Random.Range(-3f, 4f);
                }
                else
                {
                    recail = 0;
                }

                GameObject bullet = Instantiate(bulletPrefab, shootPosition.transform.position, shootPosition.transform.rotation * Quaternion.Euler(0, recail, 0));

                bullet.GetComponent<Bullet>().damageValue(playerWeapon.GetDamage());
                bullet.tag = "playerBullet";
                bullet.layer = 16;

                anim.Play("Attack");
            }

            cadenceTimer = 0;
        }

        if (!Input.GetButton("Fire1")) robotStates = RobotStates.idle;
    }

    void Dead()
    {
        anim.Play("Death");
        rigi.velocity = new Vector3(0, 0, 0);
    }

    public void FinishGame()
    {
        mc.StopBSO();

        GameObject soundDead = Instantiate(soundDeadPrefab, transform.position, transform.rotation);
        Destroy(soundDead, 2f);

        GameObject soundExplosion = Instantiate(soundExplosionPrefab, transform.position, transform.rotation);
        Destroy(soundExplosion, 1f);

        GameObject explosion = Instantiate(explosionPrefab, transform.position, transform.rotation);
        gui.OpenRetryMenu();
        Destroy(gameObject);
    }

    private void Move()
    {
        rightMove = right * Time.deltaTime * horizontal;
        forwardMove = forward * Time.deltaTime * vertical;

        rightMove = Vector3.Normalize(rightMove);
        forwardMove = Vector3.Normalize(forwardMove);

        moveDirection = (rightMove + forwardMove);

        rigi.velocity = new Vector3(moveDirection.x, 0, moveDirection.z);
        rigi.velocity = Vector3.Normalize(rigi.velocity) * speed;
    }

    private void CheckDirection()
    {
        if (horizontal == 0 && vertical == 1) yRotation = Camera.main.transform.eulerAngles.y + 0f;
        if (horizontal == 1 && vertical == 1) yRotation = Camera.main.transform.eulerAngles.y + 45f;
        if (horizontal == -1 && vertical == 1) yRotation = Camera.main.transform.eulerAngles.y + 315f;
        if (horizontal == 1 && vertical == 0) yRotation = Camera.main.transform.eulerAngles.y + 90f;
        if (horizontal == -1 && vertical == 0) yRotation = Camera.main.transform.eulerAngles.y + 270f;
        if (horizontal == 0 && vertical == -1) yRotation = Camera.main.transform.eulerAngles.y + 180f;
        if (horizontal == 1 && vertical == -1) yRotation = Camera.main.transform.eulerAngles.y + 135f;
        if (horizontal == -1 && vertical == -1) yRotation = Camera.main.transform.eulerAngles.y + 215f;
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag.Equals("PowerUp"))
        {
            SoundPU();

            PowerUpController puc = other.gameObject.GetComponent<PowerUpController>();
            playerWeapon.weapon = puc.GetWeapon();
            Destroy(other.gameObject);
        }

        if (other.gameObject.tag.Equals("PUShield"))
        {
            if (temporalShield != null)
            {
                Destroy(temporalShield);
            }

            SoundPU();

            GameObject shield = Instantiate(shieldPrefab, robotBody.transform.position, robotBody.transform.rotation);
            shield.transform.SetParent(transform, true);
            temporalShield = shield;

            Destroy(shield, 15);
            Destroy(other.gameObject);
        }

        if (other.gameObject.tag.Equals("PULife"))
        {
            SoundPU();

            healthController.Heal(50);
            Destroy(other.gameObject);
        }
    }

    private void SoundPU()
    {
        GameObject soundPU = Instantiate(soundPUPrefab, transform.position, transform.rotation);
        Destroy(soundPU, 1f);
    }

    public void AddScore(float scr)
    {
        score += scr;
        scoreTxt.text = score.ToString();
    }
}