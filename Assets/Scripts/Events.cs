﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Events : MonoBehaviour
{
    public PlayerController pc;
    public void Dead()
    {
        pc.FinishGame();
    }
}
