﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Weapons : MonoBehaviour
{
    public enum WeaponType { noWeapon, pistol, machineGun, shotgun }
    public WeaponType weapon;

    public float GetCadency()
    {
        float cadence;

        switch (weapon)
        {
            case WeaponType.pistol:
                cadence = .6f;
                break;

            case WeaponType.machineGun:
                cadence = .1f;
                break;

            case WeaponType.shotgun:
                cadence = .7f;
                break;

            default:
                cadence = 0f;
                break;
        }

        return cadence;
    }
    public float GetDamage()
    {
        float damage;

        switch (weapon)
        {
            case WeaponType.pistol:
                damage = Random.Range(20f, 25f);
                break;

            case WeaponType.machineGun:
                damage = Random.Range(5f, 9f);
                break;

            case WeaponType.shotgun:
                damage = Random.Range(10f, 19f);
                break;

            default:
                damage = 0f;
                break;
        }

        return damage;
    }
}