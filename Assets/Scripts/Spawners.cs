﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawners : MonoBehaviour
{
    private float powerUpContador, powerUpContadorMax, distanceEnemieContador, distanceEnemieContadorMax, bombEnemieContador, bombEnemieContadorMax;

    public Transform[] powerUpPositions;
    public GameObject powerUp;

    public Transform[] distanceEnemiesPositions;
    public GameObject distanceEnemie;

    public Transform[] bombEnemiesPositions;
    public GameObject bombEnemie;

    public GameObject enviro;

    private void Start()
    {
        powerUpContador = 0;
        powerUpContadorMax = Random.Range(3f, 8f);

        distanceEnemieContador = 0;
        distanceEnemieContadorMax = Random.Range(.1f, .5f);

        bombEnemieContador = 0;
        bombEnemieContadorMax = Random.Range(1f, 5f);
    }

    private void Update()
    {
        if (powerUpContador <= powerUpContadorMax)
        {
            powerUpContador += Time.deltaTime;
        }
        else
        {
            powerUpContador = 0;
            powerUpContadorMax = Random.Range(5f, 13f);

            int propRandom = Random.Range(0, 4);

            GameObject pU = Instantiate(powerUp, powerUpPositions[propRandom].transform.position, powerUpPositions[propRandom].transform.rotation);
            //pU.transform.parent = enviro.transform;
        }

        if (distanceEnemieContador <= distanceEnemieContadorMax)
        {
            distanceEnemieContador += Time.deltaTime;
        }
        else
        {
            distanceEnemieContador = 0;
            distanceEnemieContadorMax = Random.Range(4f, 8f);

            int propRandom = Random.Range(0, 4);

            GameObject dE = Instantiate(distanceEnemie, distanceEnemiesPositions[propRandom].transform.position, distanceEnemiesPositions[propRandom].transform.rotation);
            //dE.transform.parent = enviro.transform;
        }

        if (bombEnemieContador <= bombEnemieContadorMax)
        {
            bombEnemieContador += Time.deltaTime;
        }
        else
        {
            bombEnemieContador = 0;
            bombEnemieContadorMax = Random.Range(2f, 6f);

            int propRandom = Random.Range(0, 4);

            GameObject bE = Instantiate(bombEnemie, bombEnemiesPositions[propRandom].transform.position, bombEnemiesPositions[propRandom].transform.rotation);
            //bE.transform.parent = enviro.transform;
        }
    }
}