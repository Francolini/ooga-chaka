﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusicController : MonoBehaviour
{
    public AudioSource bso;

    private bool stopBSO;

    void Start()
    {
        stopBSO = false;
    }

    void Update()
    {
        if (stopBSO)
        {
            bso.volume -= Time.deltaTime / 10f;
        }
    }

    public void StartBSO()
    {
        bso.enabled = true;
    }

    public void StopBSO()
    {
        stopBSO = true;
    }
}