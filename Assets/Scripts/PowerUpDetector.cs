﻿
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerUpDetector : MonoBehaviour
{
    private GameObject powerUp;
    public EnemyController enemyC;

    private void Update()
    {
        transform.position = enemyC.gameObject.transform.position;
        transform.rotation = enemyC.gameObject.transform.rotation;
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.gameObject.tag.Equals("PowerUp") && powerUp == null && enemyC.enemyWeapon.weapon == Weapons.WeaponType.pistol)
        {
            powerUp = other.gameObject;
            enemyC.SetPowerUp(powerUp);
        }
    }
}