﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GUIController : MonoBehaviour
{
    public GameObject spawners, player, retry, inGame;
    public Text scoreText, totalScore;

    public void ActivateObjects()
    {
        player.SetActive(true);
        spawners.SetActive(true);
    }

    public void OpenRetryMenu()
    {
        Invoke("OpenGUI", 1f);
    }

    public void RestartGame()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    public void Quit()
    {
        Application.Quit();
    }

    void OpenGUI()
    {
        totalScore.text = scoreText.text;
        inGame.SetActive(false);
        spawners.SetActive(false);
        retry.SetActive(true);
    }
}
