﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shield : MonoBehaviour
{
    public GameObject ripplesPrefab, soundHieldPrefab;

    private Material mat;

    // Start is called before the first frame update
    private void OnCollisionEnter(Collision other)
    {
        if(other.gameObject.tag == "enemyBullet")
        {
            GameObject ripples = Instantiate(ripplesPrefab, transform);
            ripples.transform.parent = transform;
            ParticleSystemRenderer psr = ripples.transform.GetChild(0).GetComponent<ParticleSystemRenderer>();

            mat = psr.material;
            mat.SetVector("_SphereCenter", other.contacts[0].point);

            Destroy(ripples, 1.5f);

            GameObject soundShield = Instantiate(soundHieldPrefab, transform.position, transform.rotation);
            Destroy(soundShield, 0.9f);
        }
    }
}
